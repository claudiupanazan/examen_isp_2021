package examen.exercise2;

import javax.swing.*;

public class GUI extends JFrame {
    private final int WIDTH_SIZE = 100;
    private final int HEIGHT_SIZE = 20;

    private JLabel sentenceLabel;
    private JLabel wordsLabel;
    private JLabel charactersLabel;

    private JTextArea sentencesArea;
    private JTextArea wordsArea;
    private JTextArea charactersArea;

    private JButton calculateButton;

    public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(400, 240);
        this.setLayout(null);

        init();
    }

    void init() {
        sentenceLabel = new JLabel("Propozitie");
        sentenceLabel.setBounds(40, 40, WIDTH_SIZE, HEIGHT_SIZE);

        sentencesArea = new JTextArea();
        sentencesArea.setBounds(sentenceLabel.getX() + WIDTH_SIZE, sentenceLabel.getY(), WIDTH_SIZE * 2, HEIGHT_SIZE);

        calculateButton = new JButton("Calculeaza");
        calculateButton.setBounds(sentenceLabel.getX(), sentencesArea.getY() + HEIGHT_SIZE * 2, WIDTH_SIZE, HEIGHT_SIZE);
        calculateButton.addActionListener(e -> {
            String[] wordsCount = sentencesArea.getText().split(" ");

            if (sentencesArea.getText().length() < 1) {
                wordsArea.setText(String.valueOf(0));
            } else {
                wordsArea.setText(String.valueOf(wordsCount.length));
            }
            int charactersAppariton = (int) sentencesArea.getText().chars().filter(ch -> ch != ' ' ).count();

            charactersArea.setText(String.valueOf(charactersAppariton));
        });

        wordsLabel = new JLabel("Caracterul a");
        wordsLabel.setBounds(calculateButton.getX(), calculateButton.getY() + HEIGHT_SIZE * 2, WIDTH_SIZE, HEIGHT_SIZE);

        wordsArea = new JTextArea();
        wordsArea.setBounds(wordsLabel.getX() + WIDTH_SIZE, wordsLabel.getY(), WIDTH_SIZE, HEIGHT_SIZE);

        charactersLabel = new JLabel("Caractere");
        charactersLabel.setBounds(wordsLabel.getX(), wordsLabel.getY() + HEIGHT_SIZE * 2, WIDTH_SIZE, HEIGHT_SIZE);

        charactersArea = new JTextArea();
        charactersArea.setBounds(charactersLabel.getX() + WIDTH_SIZE, charactersLabel.getY(), WIDTH_SIZE, HEIGHT_SIZE);

        add(charactersLabel);
        add(charactersArea);
        add(wordsArea);
        add(wordsLabel);
        add(calculateButton);
        add(sentencesArea);
        add(sentenceLabel);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new GUI();
    }
}
